<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AppVersionConfig */

$this->title = Yii::t('app', 'Create App Version Config');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'App Version Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-version-config-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
