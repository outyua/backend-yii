<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppVersions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-versions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'version')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'platform')->dropDownList(\app\models\AppPlatforms::getAllPlatforms2Array()) ?>

    <?= $form->field($model, 'update_type')->dropDownList(\app\models\AppVersions::$allUpdateType) ?>

    <?= $form->field($model, 'update_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'config_id')->dropDownList(\app\models\AppVersionConfig::getAllConfigs2Array()) ?>

    <?= $form->field($model, 'status')->dropDownList(\app\models\AppVersions::$allStatus) ?>

    <?= $form->field($model, 'log')->textarea(['rows' => 10]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
