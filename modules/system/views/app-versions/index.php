<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\system\models\AppVersionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'App Versions');
$this->params['breadcrumbs'][] = $this->title;

$platforms = \app\models\AppPlatforms::getAllPlatforms2Array();
$configs = \app\models\AppVersionConfig::getAllConfigs2Array();

?>
<div class="app-versions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create App Versions'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'version',
            ['attribute'=>'platform', 'value'=>function($model) use ($platforms) {
                return $platforms[$model->platform] ?? '';
            }, 'filter'=>$platforms],
//            'log:ntext',
            ['attribute'=>'update_type', 'value'=>function($model) {
                return $model->getUpdateTypeLabel();
            }, 'filter'=>\app\models\AppVersions::$allUpdateType],
             ['attribute'=>'published_at', 'format'=>['datetime', 'php:Y-m-d H:m:s']],
             'update_url:url',
            ['attribute'=>'config_id', 'value'=>function($model) use ($configs) {
                return $configs[$model->config_id] ?? '';
            }, 'filter'=>$configs],
             ['attribute'=>'status', 'value'=>function($model) {
                return $model->getStatusLabel();
             }, 'filter'=>\app\models\AppVersions::$allStatus],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
