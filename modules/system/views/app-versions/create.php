<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AppVersions */

$this->title = Yii::t('app', 'Create App Versions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'App Versions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-versions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
