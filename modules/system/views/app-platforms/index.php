<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\system\models\AppPlatformsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'App Platforms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-platforms-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create App Platforms'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'sort',
            'description:ntext',
            ['attribute'=>'status', 'value'=>function($model) {
                return $model->getStatusLabel();
            }, 'filter'=>\app\models\AppPlatforms::$allStatus],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
