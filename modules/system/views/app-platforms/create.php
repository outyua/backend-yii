<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AppPlatforms */

$this->title = Yii::t('app', 'Create App Platforms');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'App Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-platforms-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
