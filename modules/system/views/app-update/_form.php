<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppUpdate */
/* @var $form yii\widgets\ActiveForm */

$allVersions = \app\models\AppVersions::getAllVersions2Array();
?>

<div class="app-update-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'start_version')->dropDownList($allVersions) ?>

    <?= $form->field($model, 'end_version')->dropDownList($allVersions) ?>

    <?= $form->field($model, 'markup')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
