<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\system\models\AppUpdateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'App Updates');
$this->params['breadcrumbs'][] = $this->title;

$allVersions = \app\models\AppVersions::getAllVersions2Array();
?>
<div class="app-update-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create App Update'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            ['attribute'=>'start_version', 'value'=>function($model) use ($allVersions) {
                return $allVersions[$model->start_version] ?? '';
            }, 'filter'=>$allVersions],
            ['attribute'=>'end_version', 'value'=>function($model) use ($allVersions) {
                return $allVersions[$model->end_version] ?? '';
            }, 'filter'=>$allVersions],
            ['attribute'=>'push_time', 'format'=>['datetime', 'php:Y-m-d H:m:s']],
            'markup',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
