<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AppUpdate */

$this->title = Yii::t('app', 'Create App Update');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'App Updates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-update-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
