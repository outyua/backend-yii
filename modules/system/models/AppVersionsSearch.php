<?php

namespace app\modules\system\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppVersions;

/**
 * AppVersionsSearch represents the model behind the search form about `app\models\AppVersions`.
 */
class AppVersionsSearch extends AppVersions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'version', 'platform', 'log', 'update_url'], 'safe'],
            [['update_type', 'published_at', 'config_id', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppVersions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'update_type' => $this->update_type,
            'published_at' => $this->published_at,
            'config_id' => $this->config_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'platform', $this->platform])
            ->andFilterWhere(['like', 'log', $this->log])
            ->andFilterWhere(['like', 'update_url', $this->update_url]);

        return $dataProvider;
    }
}
