<?php
/**
 * Created by PhpStorm.
 * User: wanglv
 * Date: 2017/5/16
 * Time: 16:02
 */

return [
    'Username' => '用户名',
    'Password' => '密  码',
    'Remember Me' => '记住密码',
    'Avatar' => '头像',
    'Admins' => '管理员',
    'Status' => '状态',
    'Create Admin' => '添加管理员',
    'Key' => '键',
    'Value' => '值',
    'Settings' => '设置',
    'Create Settings' => '添加设置',
    'Create' => '添加',
    'Update' => '更新',
    'Delete' => '删除',
    'App Platforms' => '渠道管理',
    'Title' => '标题',
    'Sort' => '排序',
    'Description' => '描述',
    'Create App Platforms' => '添加渠道',

    'App Versions' => '版本管理',
    'Create App Versions' => '添加版本',
    'Version' => '版本',
    'Platform' => '渠道',
    'Log' => '版本日志',
    'Update Type' => '更新类型',
    'Published At' => '发布日期',
    'Update Url' => '下载地址',
    'Config ID' => '配置',
    'App Version Configs' => '版本配置',
    'Create App Version Config' => '添加配置',
    'Content' => '内容',
    'Condition' => '前置需求',
    'Update App Versions' => '更新版本',
    'App Updates' => 'APP升级',
    'Create App Update' => '增加升级',
    'Start Version' => '开始版本',
    'End Version' => '结束版本',
    'Push Time' => '推送时间',
    'Markup' => '备注',
    'Image Sizes' => '图片尺寸',
    'Create Image Size' => '添加尺寸',
    'Type' => '类型'

];