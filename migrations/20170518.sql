﻿SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS  `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `avatar` varchar(128) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `last_login_ip` varchar(16) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `access_token` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

insert into `admin`(`id`,`username`,`password`,`avatar`,`status`,`last_login_ip`,`created_at`,`updated_at`,`access_token`) values
('2','admin','$2y$10$Id4P.znKRZiO9qjO5qeRf.yTgx5GNHQGh2nKADJQf7UDRt771hppG','/img/avatar.jpg','0','127.0.0.1','1494990669','1494990669',null);
DROP TABLE IF EXISTS  `app_platforms`;
CREATE TABLE `app_platforms` (
  `id` varchar(64) NOT NULL,
  `title` varchar(128) NOT NULL,
  `sort` int(11) DEFAULT '0',
  `description` text,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS  `app_update`;
CREATE TABLE `app_update` (
  `id` varchar(32) NOT NULL,
  `start_version` varchar(32) NOT NULL,
  `end_version` varchar(32) NOT NULL,
  `push_time` int(11) DEFAULT NULL,
  `markup` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS  `app_version_config`;
CREATE TABLE `app_version_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `content` text NOT NULL COMMENT '配置的详情 json',
  `condition` text COMMENT '使用该配置需要的前提条件 json',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS  `app_versions`;
CREATE TABLE `app_versions` (
  `id` varchar(32) NOT NULL,
  `version` varchar(32) NOT NULL,
  `platform` varchar(64) NOT NULL,
  `log` text,
  `update_type` tinyint(4) NOT NULL DEFAULT '0',
  `published_at` int(11) DEFAULT NULL,
  `update_url` varchar(512) DEFAULT NULL,
  `config_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS  `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into `auth_assignment`(`item_name`,`user_id`,`created_at`) values
('超级管理员','2','1494997230');
DROP TABLE IF EXISTS  `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into `auth_item`(`name`,`type`,`description`,`rule_name`,`created_at`,`updated_at`) values
('/*','2',null,null,'1495004453','1495004453'),
('/admin/*','2',null,null,'1494993659','1494993659'),
('/admin/assignment/*','2',null,null,'1494993659','1494993659'),
('/admin/assignment/assign','2',null,null,'1494993659','1494993659'),
('/admin/assignment/index','2',null,null,'1494993659','1494993659'),
('/admin/assignment/revoke','2',null,null,'1494993659','1494993659'),
('/admin/assignment/view','2',null,null,'1494993659','1494993659'),
('/admin/default/*','2',null,null,'1494993659','1494993659'),
('/admin/default/index','2',null,null,'1494993659','1494993659'),
('/admin/menu/*','2',null,null,'1494993659','1494993659'),
('/admin/menu/create','2',null,null,'1494993659','1494993659'),
('/admin/menu/delete','2',null,null,'1494993659','1494993659'),
('/admin/menu/index','2',null,null,'1494993659','1494993659'),
('/admin/menu/update','2',null,null,'1494993659','1494993659'),
('/admin/menu/view','2',null,null,'1494993659','1494993659'),
('/admin/permission/*','2',null,null,'1494993659','1494993659'),
('/admin/permission/assign','2',null,null,'1494993659','1494993659'),
('/admin/permission/create','2',null,null,'1494993659','1494993659'),
('/admin/permission/delete','2',null,null,'1494993659','1494993659'),
('/admin/permission/index','2',null,null,'1494993659','1494993659'),
('/admin/permission/remove','2',null,null,'1494993659','1494993659'),
('/admin/permission/update','2',null,null,'1494993659','1494993659'),
('/admin/permission/view','2',null,null,'1494993659','1494993659'),
('/admin/role/*','2',null,null,'1494993659','1494993659'),
('/admin/role/assign','2',null,null,'1494993659','1494993659'),
('/admin/role/create','2',null,null,'1494993659','1494993659'),
('/admin/role/delete','2',null,null,'1494993659','1494993659'),
('/admin/role/index','2',null,null,'1494993659','1494993659'),
('/admin/role/remove','2',null,null,'1494993659','1494993659'),
('/admin/role/update','2',null,null,'1494993659','1494993659'),
('/admin/role/view','2',null,null,'1494993659','1494993659'),
('/admin/route/*','2',null,null,'1494993659','1494993659'),
('/admin/route/assign','2',null,null,'1494993659','1494993659'),
('/admin/route/create','2',null,null,'1494993659','1494993659'),
('/admin/route/index','2',null,null,'1494993659','1494993659'),
('/admin/route/refresh','2',null,null,'1494993659','1494993659'),
('/admin/route/remove','2',null,null,'1494993659','1494993659'),
('/admin/rule/*','2',null,null,'1494993659','1494993659'),
('/admin/rule/create','2',null,null,'1494993659','1494993659'),
('/admin/rule/delete','2',null,null,'1494993659','1494993659'),
('/admin/rule/index','2',null,null,'1494993659','1494993659'),
('/admin/rule/update','2',null,null,'1494993659','1494993659'),
('/admin/rule/view','2',null,null,'1494993659','1494993659'),
('/admin/user/*','2',null,null,'1494993659','1494993659'),
('/admin/user/activate','2',null,null,'1494993659','1494993659'),
('/admin/user/change-password','2',null,null,'1494993659','1494993659'),
('/admin/user/delete','2',null,null,'1494993659','1494993659'),
('/admin/user/index','2',null,null,'1494993659','1494993659'),
('/admin/user/login','2',null,null,'1494993659','1494993659'),
('/admin/user/logout','2',null,null,'1494993659','1494993659'),
('/admin/user/request-password-reset','2',null,null,'1494993659','1494993659'),
('/admin/user/reset-password','2',null,null,'1494993659','1494993659'),
('/admin/user/signup','2',null,null,'1494993659','1494993659'),
('/admin/user/view','2',null,null,'1494993659','1494993659'),
('/debug/*','2',null,null,'1495004453','1495004453'),
('/debug/default/*','2',null,null,'1495004453','1495004453'),
('/debug/default/db-explain','2',null,null,'1495004453','1495004453'),
('/debug/default/download-mail','2',null,null,'1495004453','1495004453'),
('/debug/default/index','2',null,null,'1495004453','1495004453'),
('/debug/default/toolbar','2',null,null,'1495004453','1495004453'),
('/debug/default/view','2',null,null,'1495004453','1495004453'),
('/gii/*','2',null,null,'1495004453','1495004453'),
('/gii/default/*','2',null,null,'1495004453','1495004453'),
('/gii/default/action','2',null,null,'1495004453','1495004453'),
('/gii/default/diff','2',null,null,'1495004453','1495004453'),
('/gii/default/index','2',null,null,'1495004453','1495004453'),
('/gii/default/preview','2',null,null,'1495004453','1495004453'),
('/gii/default/view','2',null,null,'1495004453','1495004453'),
('/site/*','2',null,null,'1495004453','1495004453'),
('/site/about','2',null,null,'1495004453','1495004453'),
('/site/captcha','2',null,null,'1495004453','1495004453'),
('/site/contact','2',null,null,'1495004453','1495004453'),
('/site/error','2',null,null,'1495004453','1495004453'),
('/site/index','2',null,null,'1495004453','1495004453'),
('/site/login','2',null,null,'1495004453','1495004453'),
('/site/logout','2',null,null,'1495004453','1495004453'),
('/system/*','2',null,null,'1494998219','1494998219'),
('/system/admin/*','2',null,null,'1494998219','1494998219'),
('/system/admin/create','2',null,null,'1494998219','1494998219'),
('/system/admin/delete','2',null,null,'1494998219','1494998219'),
('/system/admin/index','2',null,null,'1494998219','1494998219'),
('/system/admin/update','2',null,null,'1494998219','1494998219'),
('/system/admin/view','2',null,null,'1494998219','1494998219'),
('/system/app-platforms/*','2',null,null,'1495091014','1495091014'),
('/system/app-platforms/create','2',null,null,'1495091014','1495091014'),
('/system/app-platforms/delete','2',null,null,'1495091014','1495091014'),
('/system/app-platforms/index','2',null,null,'1495091014','1495091014'),
('/system/app-platforms/update','2',null,null,'1495091014','1495091014'),
('/system/app-platforms/view','2',null,null,'1495091014','1495091014'),
('/system/app-update/*','2',null,null,'1495091014','1495091014'),
('/system/app-update/create','2',null,null,'1495091014','1495091014'),
('/system/app-update/delete','2',null,null,'1495091014','1495091014'),
('/system/app-update/index','2',null,null,'1495091014','1495091014'),
('/system/app-update/update','2',null,null,'1495091014','1495091014'),
('/system/app-update/view','2',null,null,'1495091014','1495091014'),
('/system/app-version-config/*','2',null,null,'1495091015','1495091015'),
('/system/app-version-config/create','2',null,null,'1495091015','1495091015'),
('/system/app-version-config/delete','2',null,null,'1495091015','1495091015'),
('/system/app-version-config/index','2',null,null,'1495091014','1495091014'),
('/system/app-version-config/update','2',null,null,'1495091015','1495091015'),
('/system/app-version-config/view','2',null,null,'1495091015','1495091015'),
('/system/app-versions/*','2',null,null,'1495091015','1495091015'),
('/system/app-versions/create','2',null,null,'1495091015','1495091015'),
('/system/app-versions/delete','2',null,null,'1495091015','1495091015'),
('/system/app-versions/index','2',null,null,'1495091015','1495091015'),
('/system/app-versions/update','2',null,null,'1495091015','1495091015'),
('/system/app-versions/view','2',null,null,'1495091015','1495091015'),
('/system/default/*','2',null,null,'1494998219','1494998219'),
('/system/default/index','2',null,null,'1494998219','1494998219'),
('/system/image-size/*','2',null,null,'1495097006','1495097006'),
('/system/image-size/create','2',null,null,'1495097006','1495097006'),
('/system/image-size/delete','2',null,null,'1495097006','1495097006'),
('/system/image-size/index','2',null,null,'1495097005','1495097005'),
('/system/image-size/update','2',null,null,'1495097006','1495097006'),
('/system/image-size/view','2',null,null,'1495097005','1495097005'),
('/system/settings/*','2',null,null,'1495002430','1495002430'),
('/system/settings/create','2',null,null,'1495002430','1495002430'),
('/system/settings/delete','2',null,null,'1495002430','1495002430'),
('/system/settings/index','2',null,null,'1495002430','1495002430'),
('/system/settings/update','2',null,null,'1495002430','1495002430'),
('/system/settings/view','2',null,null,'1495002430','1495002430'),
('全部权限','2',null,null,'1494997153','1494997153'),
('权限控制','2',null,null,'1494997002','1494997002'),
('超级管理员','1',null,null,'1494997079','1494997079');
DROP TABLE IF EXISTS  `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into `auth_item_child`(`parent`,`child`) values
('全部权限','/*'),
('全部权限','/admin/*'),
('全部权限','/admin/assignment/*'),
('权限控制','/admin/assignment/*'),
('全部权限','/admin/assignment/assign'),
('权限控制','/admin/assignment/assign'),
('全部权限','/admin/assignment/index'),
('权限控制','/admin/assignment/index'),
('全部权限','/admin/assignment/revoke'),
('权限控制','/admin/assignment/revoke'),
('全部权限','/admin/assignment/view'),
('权限控制','/admin/assignment/view'),
('全部权限','/admin/default/*'),
('全部权限','/admin/default/index'),
('全部权限','/admin/menu/*'),
('权限控制','/admin/menu/*'),
('全部权限','/admin/menu/create'),
('权限控制','/admin/menu/create'),
('全部权限','/admin/menu/delete'),
('权限控制','/admin/menu/delete'),
('全部权限','/admin/menu/index'),
('权限控制','/admin/menu/index'),
('全部权限','/admin/menu/update'),
('权限控制','/admin/menu/update'),
('全部权限','/admin/menu/view'),
('权限控制','/admin/menu/view'),
('全部权限','/admin/permission/*'),
('权限控制','/admin/permission/*'),
('全部权限','/admin/permission/assign'),
('权限控制','/admin/permission/assign'),
('全部权限','/admin/permission/create'),
('权限控制','/admin/permission/create'),
('全部权限','/admin/permission/delete'),
('权限控制','/admin/permission/delete'),
('全部权限','/admin/permission/index'),
('权限控制','/admin/permission/index'),
('全部权限','/admin/permission/remove'),
('权限控制','/admin/permission/remove'),
('全部权限','/admin/permission/update'),
('权限控制','/admin/permission/update'),
('全部权限','/admin/permission/view'),
('权限控制','/admin/permission/view'),
('全部权限','/admin/role/*'),
('权限控制','/admin/role/*'),
('全部权限','/admin/role/assign'),
('权限控制','/admin/role/assign'),
('全部权限','/admin/role/create'),
('权限控制','/admin/role/create'),
('全部权限','/admin/role/delete'),
('权限控制','/admin/role/delete'),
('全部权限','/admin/role/index'),
('权限控制','/admin/role/index'),
('全部权限','/admin/role/remove'),
('权限控制','/admin/role/remove'),
('全部权限','/admin/role/update'),
('权限控制','/admin/role/update'),
('全部权限','/admin/role/view'),
('权限控制','/admin/role/view'),
('全部权限','/admin/route/*'),
('权限控制','/admin/route/*'),
('全部权限','/admin/route/assign'),
('权限控制','/admin/route/assign'),
('全部权限','/admin/route/create'),
('权限控制','/admin/route/create'),
('全部权限','/admin/route/index'),
('权限控制','/admin/route/index'),
('全部权限','/admin/route/refresh'),
('权限控制','/admin/route/refresh'),
('全部权限','/admin/route/remove'),
('权限控制','/admin/route/remove'),
('全部权限','/admin/rule/*'),
('权限控制','/admin/rule/*'),
('全部权限','/admin/rule/create'),
('权限控制','/admin/rule/create'),
('全部权限','/admin/rule/delete'),
('权限控制','/admin/rule/delete'),
('全部权限','/admin/rule/index'),
('权限控制','/admin/rule/index'),
('全部权限','/admin/rule/update'),
('权限控制','/admin/rule/update'),
('全部权限','/admin/rule/view'),
('权限控制','/admin/rule/view'),
('全部权限','/admin/user/*'),
('全部权限','/admin/user/activate'),
('全部权限','/admin/user/change-password'),
('全部权限','/admin/user/delete'),
('全部权限','/admin/user/index'),
('全部权限','/admin/user/login'),
('全部权限','/admin/user/logout'),
('全部权限','/admin/user/request-password-reset'),
('全部权限','/admin/user/reset-password'),
('全部权限','/admin/user/signup'),
('全部权限','/admin/user/view'),
('全部权限','/debug/*'),
('全部权限','/debug/default/*'),
('全部权限','/debug/default/db-explain'),
('全部权限','/debug/default/download-mail'),
('全部权限','/debug/default/index'),
('全部权限','/debug/default/toolbar'),
('全部权限','/debug/default/view'),
('全部权限','/gii/*'),
('全部权限','/gii/default/*'),
('全部权限','/gii/default/action'),
('全部权限','/gii/default/diff'),
('全部权限','/gii/default/index'),
('全部权限','/gii/default/preview'),
('全部权限','/gii/default/view'),
('全部权限','/site/*'),
('全部权限','/site/about'),
('全部权限','/site/captcha'),
('全部权限','/site/contact'),
('全部权限','/site/error'),
('全部权限','/site/index'),
('全部权限','/site/login'),
('全部权限','/site/logout'),
('全部权限','/system/*'),
('全部权限','/system/admin/*'),
('全部权限','/system/admin/create'),
('全部权限','/system/admin/delete'),
('全部权限','/system/admin/index'),
('全部权限','/system/admin/update'),
('全部权限','/system/admin/view'),
('全部权限','/system/app-platforms/*'),
('全部权限','/system/app-platforms/create'),
('全部权限','/system/app-platforms/delete'),
('全部权限','/system/app-platforms/index'),
('全部权限','/system/app-platforms/update'),
('全部权限','/system/app-platforms/view'),
('全部权限','/system/app-update/*'),
('全部权限','/system/app-update/create'),
('全部权限','/system/app-update/delete'),
('全部权限','/system/app-update/index'),
('全部权限','/system/app-update/update'),
('全部权限','/system/app-update/view'),
('全部权限','/system/app-version-config/*'),
('全部权限','/system/app-version-config/create'),
('全部权限','/system/app-version-config/delete'),
('全部权限','/system/app-version-config/index'),
('全部权限','/system/app-version-config/update'),
('全部权限','/system/app-version-config/view'),
('全部权限','/system/app-versions/*'),
('全部权限','/system/app-versions/create'),
('全部权限','/system/app-versions/delete'),
('全部权限','/system/app-versions/index'),
('全部权限','/system/app-versions/update'),
('全部权限','/system/app-versions/view'),
('全部权限','/system/default/*'),
('全部权限','/system/default/index'),
('全部权限','/system/image-size/*'),
('全部权限','/system/image-size/create'),
('全部权限','/system/image-size/delete'),
('全部权限','/system/image-size/index'),
('全部权限','/system/image-size/update'),
('全部权限','/system/image-size/view'),
('全部权限','/system/settings/*'),
('全部权限','/system/settings/create'),
('全部权限','/system/settings/delete'),
('全部权限','/system/settings/index'),
('全部权限','/system/settings/update'),
('全部权限','/system/settings/view'),
('超级管理员','全部权限');
DROP TABLE IF EXISTS  `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS  `image_size`;
CREATE TABLE `image_size` (
  `id` varchar(32) NOT NULL,
  `title` varchar(64) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS  `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(128) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

insert into `menu`(`id`,`name`,`parent`,`route`,`order`,`data`) values
('1','权限设置',null,null,'1000','{
    "icon":"id-card-o"
}'),
('2','路由','1','/admin/route/index','1','{
    "icon" : "usb"
}'),
('3','菜单','1','/admin/menu/index','5','{
    "icon":"bars"
}'),
('4','角色','1','/admin/role/index','4','{
    "icon":"vcard-o"
}'),
('5','分配','1','/admin/assignment/index','3','{
    "icon":"random"
}'),
('6','用户','1','/system/admin/index','999','{
    "icon":"user"
}'),
('7','权限','1','/admin/permission/index','2','{
    "icon":"hand-paper-o"
}'),
('8','系统设置',null,null,'997','{
"icon":"cog"
}'),
('9','平台版本',null,null,'996','{
"icon":"tasks"
}'),
('10','渠道管理','9','/system/app-platforms/index','1','{
"icon":"sliders"
}'),
('11',' 版本管理','9','/system/app-versions/index','2','{
"icon":"code-fork"
}'),
('12','版本配置','9','/system/app-version-config/index','3','{
"icon":"cogs"
}'),
('13','APP升级','9','/system/app-update/index','4','{
"icon":"level-up"
}'),
('14','全局配置','8','/system/settings/index',null,null),
('15','图片尺寸','8','/system/image-size/index',null,null);
DROP TABLE IF EXISTS  `settings`;
CREATE TABLE `settings` (
  `key` varchar(64) NOT NULL,
  `value` text,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into `settings`(`key`,`value`) values
('copyright','版权所有 OUTYUA');
DROP TABLE IF EXISTS  `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(256) NOT NULL,
  `password_reset_token` varchar(256) DEFAULT NULL,
  `email` varchar(256) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

insert into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`) values
('1','admin','CFgyIHxkVz74hLJ0Q4xipQ97jzMAhxLK','$2y$13$X/3CyA/e4Dv0TKRNmdNPBup6nDjDuViqYN7/pCKPusT.XAjRmZ.3i',null,'admin@admin.com','10','1495005597','1495005597');
SET FOREIGN_KEY_CHECKS = 1;

