<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "image_size".
 *
 * @property string $id
 * @property string $title
 * @property integer $type
 */
class ImageSize extends \yii\db\ActiveRecord
{

    public static $allType = [
        0 => '普通图片'
    ];

    public function getTypeLabel() {
        return self::$allType[$this->type] ?? '';
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image_size';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type'], 'required'],
            [['type'], 'integer'],
            [['id'], 'string', 'max' => 32],
            [['title'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
