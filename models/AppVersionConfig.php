<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "app_version_config".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $condition
 */
class AppVersionConfig extends \yii\db\ActiveRecord
{
    private static $_allConfigsArray = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_version_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content', 'condition'], 'string'],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'condition' => Yii::t('app', 'Condition'),
        ];
    }

    public static function getAllConfigs2Array() {
        if (empty(self::$_allConfigsArray)) {
            $p = static::find()->orderBy(['title'=>SORT_ASC])->select(['id', 'title'])->asArray()->all();
            if (empty($p)) {
                return [];
            }
            self::$_allConfigsArray =  ArrayHelper::map($p, 'id', 'title');
        }
        return self::$_allConfigsArray;
    }
}
