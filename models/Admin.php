<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "admin".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $avatar
 * @property integer $status
 * @property string $last_login_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $access_token
 */
class Admin extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin';
    }
    public function beforeSave($insert)
    {
        $this->updated_at = time();
        $this->last_login_ip = Yii::$app->request->userIP;
        if (parent::beforeSave($insert)) {
            // ...custom code here...
            if ($insert) {
                $this->created_at = time();
                $this->status = 0;
                $this->avatar = '';
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username'], 'string', 'max' => 32],
            [['password', 'avatar', 'access_token'], 'string', 'max' => 128],
            [['last_login_ip'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'avatar' => Yii::t('app', 'Avatar'),
            'status' => Yii::t('app', 'Status'),
            'last_login_ip' => Yii::t('app', 'Last Login Ip'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'access_token' => Yii::t('app', 'Access Token'),
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token'=>$token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->access_token;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() == $authKey;
    }

    public static function findByUsername($username) {
        $user = self::find()->where(['username'=>$username])->one();
        if (!empty($user)) {
            return $user;
        }
        return null;
    }


    public static function encryptPassword($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function checkPassword($inputPassword) {
        return password_verify($inputPassword, $this->password);
    }
}
