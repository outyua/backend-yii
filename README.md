Yii 2 后台系统
============================

> 该后台基于 Yii2

### 环境
- PHP7 及以上版本
- MySQL 5.6及以上版本

### 使用说明
- 克隆代码到本地 `git clone git@gitlab.com:outyua/backend-yii.git`
- 修改数据库配置
- 导入数据库 `migrations/20170517.sql`
- 登录账号 `admin@admin.com` 密码 `admin`

### 已经实现功能

- 管理员登录退出
- RBAC 权限控制
- 全局KV配置
- 图片尺寸管理
- APP 渠道版本管理, APP版本升级

### 待开发功能

- 对接阿里云存储
- 对接七牛云存储

### 功能说明
- 全局 KV 配置

`Settings::set($key, $value)` 设置全局变量 `$value` 支持各种类型
`Settings::get($key, $default)` 获取全局配置

$key = 'copyright' 的值为 `footer` 版权说明


